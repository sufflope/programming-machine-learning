use std::num::ParseIntError;

use ndarray::{Array1, Array2};

pub(crate) struct History {
    days: Vec<Day>,
}

impl History {
    pub(crate) fn load(s: &str) -> Result<Self, ParseError> {
        let days: Vec<_> = s
            .lines()
            .skip(1)
            .map(Day::try_from)
            .collect::<Result<_, _>>()?;
        Ok(Self { days })
    }
}

impl From<History> for (Array2<f64>, Array1<f64>) {
    fn from(val: History) -> Self {
        (
            Array2::from_shape_fn((val.days.len(), 4), |(i, j)| {
                let day = &val.days[i];
                (match j {
                    0 => 1,
                    1 => day.reservations,
                    2 => day.temperature,
                    3 => day.tourists,
                    _ => panic!(),
                }) as f64
            }),
            Array1::from_iter(val.days.iter().map(|day| day.value as f64)),
        )
    }
}

pub(crate) struct Day {
    reservations: usize,
    temperature: usize,
    tourists: usize,
    value: usize,
}

impl TryFrom<&str> for Day {
    type Error = ParseError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value.split_whitespace().collect::<Vec<_>>().as_slice() {
            [reservations, temperature, tourists, value] => Ok(Self {
                reservations: reservations.parse()?,
                temperature: temperature.parse()?,
                tourists: tourists.parse()?,
                value: value.parse()?,
            }),
            values => Err(ParseError::TooManyValues(
                values.iter().map(|s| s.to_string()).collect(),
            )),
        }
    }
}

#[derive(Debug, thiserror::Error)]
pub(crate) enum ParseError {
    #[error("invalid value")]
    InvalidValue(#[from] ParseIntError),
    #[error("too many values: {}", .0.join(", "))]
    TooManyValues(Vec<String>),
}
