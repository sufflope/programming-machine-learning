use ndarray::{Array1, ArrayView1, ArrayView2};

use crate::model::Model;

pub trait Loss {
    fn gradient<M>(model: &M, x: ArrayView2<f64>, y: ArrayView1<f64>) -> Array1<f64>
    where
        M: Model;

    fn loss<M>(model: &M, x: ArrayView2<f64>, y: ArrayView1<f64>) -> f64
    where
        M: Model;

    fn smooth(weighted_sum: Array1<f64>) -> Array1<f64>;
}
