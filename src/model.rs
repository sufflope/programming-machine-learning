use std::{fmt, marker::PhantomData};

use ndarray::{Array1, ArrayView1, ArrayView2, Axis, Zip};

use crate::loss::Loss;

pub trait Model {
    fn forward(&self, x: ArrayView2<f64>) -> Array1<f64>;

    fn train(x: ArrayView2<f64>, y: ArrayView1<f64>, iterations: usize, lr: f64) -> Self;
}

pub trait Classifier: Model {
    fn classify(&self, x: ArrayView2<f64>) -> Array1<i64> {
        self.forward(x).mapv(|v| v.round() as i64)
    }

    fn test(&self, x: ArrayView2<f64>, y: ArrayView1<f64>) {
        let total = x.len_of(Axis(0)) as f64;
        let correct = Zip::from(&self.classify(x))
            .and(y)
            .map_collect(|&x, &y| if x == y as i64 { 1 } else { 0 })
            .sum();
        let ratio = (correct as f64 * 100.) / total;
        log::info!("Success: {correct}/{total} ({ratio:.2}%)")
    }
}

pub struct Weights<L> {
    pub w: Array1<f64>,
    loss: PhantomData<L>,
}

impl<L> Model for Weights<L>
where
    L: Loss,
{
    fn forward(&self, x: ArrayView2<f64>) -> Array1<f64> {
        L::smooth(x.dot(&self.w))
    }

    fn train(x: ArrayView2<f64>, y: ArrayView1<f64>, iterations: usize, lr: f64) -> Self {
        let mut result = Self {
            w: Array1::zeros(x.shape()[x.shape().len() - 1]),
            loss: PhantomData,
        };
        for i in 0..iterations {
            log::debug!("Iteration {i:>4} => Loss: {:.20}", L::loss(&result, x, y));
            let gradient = L::gradient(&result, x, y);
            result.w = result.w - gradient * lr;
        }
        result
    }
}

impl<L> fmt::Display for Weights<L> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Weights: {:.8}", self.w)
    }
}
