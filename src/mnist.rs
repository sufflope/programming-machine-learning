use std::path::PathBuf;

use anyhow::anyhow;
use mnist::MnistBuilder;
use ndarray::{Array1, Array2};

pub(crate) struct Mnist {
    pub train: Set,
    pub test: Set,
}

impl Mnist {
    fn new(path: PathBuf, train_set_length: u32, test_set_length: u32) -> anyhow::Result<Self> {
        let mnist = MnistBuilder::new()
            .base_path(path.to_str().ok_or(anyhow!("Path must be valid unicode"))?)
            .download_and_extract()
            .training_set_length(train_set_length)
            .test_set_length(test_set_length)
            .finalize();

        fn prepend_bias(data: &[u8], len: u32) -> Array2<f64> {
            Array2::from_shape_fn((len as usize, 28 * 28 + 1), |(i, j)| {
                if j == 0 {
                    1.
                } else {
                    data[i * (28 * 28) + j - 1] as f64
                }
            })
        }

        fn encode_fives(data: &[u8], len: u32) -> anyhow::Result<Array1<f64>> {
            let result = Array1::from_shape_vec(
                len as usize,
                data.iter().map(|&v| if v == 5 { 1. } else { 0. }).collect(),
            )?;
            Ok(result)
        }

        Ok(Self {
            train: Set {
                x: prepend_bias(&mnist.trn_img, train_set_length),
                y: encode_fives(&mnist.trn_lbl, train_set_length)?,
            },
            test: Set {
                x: prepend_bias(&mnist.tst_img, test_set_length),
                y: encode_fives(&mnist.tst_lbl, test_set_length)?,
            },
        })
    }
}

impl Default for Mnist {
    fn default() -> Self {
        let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        path.push("resources/mnist/");
        Self::new(path, 60000, 10000).unwrap()
    }
}

pub(crate) struct Set {
    pub x: Array2<f64>,
    pub y: Array1<f64>,
}
