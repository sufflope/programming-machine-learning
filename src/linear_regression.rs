use ndarray::{Array1, ArrayView1, ArrayView2};

use crate::{loss::Loss, model::Model};

#[derive(Debug, Default, PartialEq)]
pub struct LinearRegression;

impl Loss for LinearRegression {
    fn gradient<M>(model: &M, x: ArrayView2<f64>, y: ArrayView1<f64>) -> Array1<f64>
    where
        M: Model,
    {
        2. * x.t().dot(&(model.forward(x) - y)) / x.nrows() as f64
    }

    fn loss<M>(model: &M, x: ArrayView2<f64>, y: ArrayView1<f64>) -> f64
    where
        M: Model,
    {
        ((model.forward(x) - y).mapv(|v| v.powi(2)))
            .mean()
            .unwrap_or(0.)
    }

    fn smooth(weighted_sum: Array1<f64>) -> Array1<f64> {
        weighted_sum
    }
}

#[cfg(test)]
mod tests {
    use approx::assert_abs_diff_eq;

    use super::*;
    use crate::{model::Weights, pizzeria::History};

    #[test]
    fn test_pizzas() {
        let history = History::load(include_str!("../resources/pizza.txt")).unwrap();

        let (x, y) = history.into();
        let regression: Weights<LinearRegression> =
            Weights::train(x.view(), y.view(), 100000, 0.001);
        assert_eq!(regression.w.len(), 4);
        for (i, weight) in [2.41178207, 1.23368396, -0.02689984, 3.12460558]
            .iter()
            .enumerate()
        {
            assert_abs_diff_eq!(regression.w[i], weight, epsilon = 1e-8);
        }

        for (i, prediction) in [45.8717, 23.2502, 28.5192, 58.2355, 42.8009]
            .iter()
            .enumerate()
        {
            let pizzas = regression.forward(x.row(i).into_shape((1, 4)).unwrap());
            assert_eq!(pizzas.len(), 1);
            assert_abs_diff_eq!(pizzas[0], prediction, epsilon = 1e-4)
        }
    }
}
