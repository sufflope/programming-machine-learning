use ndarray::{Array1, ArrayView1, ArrayView2, Axis};

use crate::{
    loss::Loss,
    model::{Classifier, Model, Weights},
};

pub struct Sigmoid;

impl Loss for Sigmoid {
    fn gradient<M>(model: &M, x: ArrayView2<f64>, y: ArrayView1<f64>) -> Array1<f64>
    where
        M: Model,
    {
        (x.t().dot(&(model.forward(x) - y))) / x.len_of(Axis(0)) as f64
    }

    fn loss<M>(model: &M, x: ArrayView2<f64>, y: ArrayView1<f64>) -> f64
    where
        M: Model,
    {
        let prediction = model.forward(x);
        let first = y.to_owned() * (&prediction.map(|v| v.ln()));
        let second = (Array1::<f64>::ones(y.raw_dim()) - y) * (&prediction.map(|v| (1. - v).ln()));
        -(first + second).mean().unwrap_or(0.)
    }

    fn smooth(weighted_sum: Array1<f64>) -> Array1<f64> {
        weighted_sum.mapv_into(|v| 1. / (1. + (-v).exp()))
    }
}

impl Classifier for Weights<Sigmoid> {}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{mnist::Mnist, model::Weights, pizzeria::History};

    #[test]
    fn test_mnist() {
        testing_logger::setup();

        let mnist = Mnist::default();
        let sigmoid: Weights<Sigmoid> =
            Weights::train(mnist.train.x.view(), mnist.train.y.view(), 100, 1e-5);
        sigmoid.test(mnist.test.x.view(), mnist.test.y.view());

        testing_logger::validate(|logs| {
            assert_eq!(
                logs.last().map(|log| log.body.as_str()),
                Some("Success: 9637/10000 (96.37%)")
            )
        })
    }

    #[test]
    fn test_police() {
        testing_logger::setup();

        let history = History::load(include_str!("../resources/police.txt")).unwrap();

        let (x, y) = history.into();
        let sigmoid: Weights<Sigmoid> = Weights::train(x.view(), y.view(), 10000, 0.001);

        sigmoid.test(x.view(), y.view());

        testing_logger::validate(|logs| {
            assert_eq!(
                logs.last().map(|log| log.body.as_str()),
                Some("Success: 25/30 (83.33%)")
            )
        })
    }
}
