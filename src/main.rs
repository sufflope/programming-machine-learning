#![warn(clippy::explicit_iter_loop)]
#![warn(clippy::flat_map_option)]
#![warn(clippy::if_then_some_else_none)]
#![warn(clippy::use_self)]

mod linear_regression;
mod loss;
mod mnist;
mod model;
mod pizzeria;
mod sigmoid;

use env_logger::Env;

use crate::{
    linear_regression::LinearRegression,
    mnist::Mnist,
    model::{Classifier, Model, Weights},
    pizzeria::History,
    sigmoid::Sigmoid,
};

fn main() -> anyhow::Result<()> {
    env_logger::init_from_env(Env::default().default_filter_or("info"));

    let history = History::load(include_str!("../resources/pizza.txt"))?;

    let (x, y) = history.into();
    let regression: Weights<LinearRegression> = Weights::train(x.view(), y.view(), 100000, 0.001);
    log::info!("{regression}");

    log::info!("A few predictions:");
    for i in 0..5 {
        let pizzas = regression.forward(x.row(i).into_shape((1, 4))?)[0];
        log::info!("X[{i}] -> {pizzas:.4} (label: {})", y[i]);
    }

    let history = History::load(include_str!("../resources/police.txt"))?;

    let (x, y) = history.into();
    let police: Weights<Sigmoid> = Weights::train(x.view(), y.view(), 10000, 0.001);
    police.test(x.view(), y.view());

    let mnist = Mnist::default();
    let characters: Weights<Sigmoid> =
        Weights::train(mnist.train.x.view(), mnist.train.y.view(), 100, 1e-5);
    log::info!("{characters}");
    characters.test(mnist.test.x.view(), mnist.test.y.view());

    Ok(())
}
